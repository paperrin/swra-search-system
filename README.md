# Star Wars Rebels Alliance Search System

My take on a job technical test.

The project had to be in 2 parts:
- a custom Node.js REST api ( Hapi.js was recommended ) able to search any data on [SWAPI.dev](http://swapi.dev).
- a React.js app using that custom api.

The use of any [SWAPI.dev](http://swapi.dev) library was forbidden.

## Sreenshots
![Search Page](https://gitlab.com/paperrin/swra-search-system/uploads/41146af8bffaf471f3e5c9595a93ad20/Search.PNG)
![Details Page](https://gitlab.com/paperrin/swra-search-system/uploads/45403feb49daef730bab8793a2c4254b/Details.PNG)

## Project setup
```
npm run install-all
```
Installs both server's and client's dependencies ( runs npm install in ./ and ./client ).

### Start
```
npm run dev
```
Runs the server and client in the development mode concurrently.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Both client and server will reload if you make edits.<br />
