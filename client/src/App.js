import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import GlobalStyle from './styled/GlobalStyle';

import Home from 'views/Home';
import Details from 'views/Details';
import SearchBar from "./components/SearchBar";

const Container = styled.div`
	height: 100%;
	width: 100%;
	
	@media screen and (min-width: 50em) {
		max-width: 50em;
		margin: auto;
	}
`;

class App extends Component {
	render() {
		const App = () => (
			<Container>
				<GlobalStyle />
				<SearchBar />
				<Switch>
					<Route exact path='/' component={Home} />
					<Route path='/details/:category/:id' component={Details} />
				</Switch>
			</Container>
		)
		return (
			<Switch>
				<App/>
			</Switch>
		);
	}
}

export default App;
