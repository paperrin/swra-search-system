import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Select from "./Select";
import {searchRequestFunc} from "../redux/actions/search";
import {connect} from "react-redux";

const SearchForm = styled.form`
	display: flex;
	background-color: #b00;
	padding: 10px;
	* {
		margin: 5px;
	}
`;
const SearchInput = styled.input`
	flex: 1;
`;

class SearchBar extends Component {
	constructor(props) {
		super(props);

		const categories = ['people', 'films', 'starships', 'vehicles', 'species', 'planets'];

		this.state = {
			inputValue: '',
			selectValue: categories[0],
			categories: categories,
		};
	}

	onSelectChange = selectValue => {
		this.setState(prevState => ({
			...prevState,
			selectValue,
		}));
	}

	onInputChange = e => {
		const inputValue = e.target.value;

		this.setState(prevState => ({
			...prevState,
			inputValue,
		}));
	}

	onFormSubmit = (e) => {
		e.preventDefault();
		const { selectValue, inputValue } = this.state;

		this.props.searchRequest(selectValue, inputValue);
		this.props.history.push(`/?category=${selectValue}&search=${inputValue}`);
	}

	render() {
		const selectOptions = this.state.categories.map(category => ({
			value: category,
			label: category,
		}));

		return (
			<SearchForm onSubmit={this.onFormSubmit}>
				<Select onChange={this.onSelectChange}
						options={selectOptions} />
				<SearchInput
					type='text'
					placeholder='Search...'
					value={this.state.inputValue}
					onChange={this.onInputChange}
				/>
				<button type='submit'>
					<FontAwesomeIcon icon={['fas','search']} />
				</button>
			</SearchForm>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	searchRequest: searchRequestFunc(dispatch),
});
export default withRouter(connect(null, mapDispatchToProps)(SearchBar));