import React, { Component } from 'react';
import styled from 'styled-components';
import QueryString from 'query-string';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { searchRequestFunc } from 'redux/actions/search';
import { Link } from "react-router-dom";

const List = styled.ul`
	flex: 1;
	overflow-y: auto;
`;
const ListItem = styled.li`
	width: 100%;
	padding: 10px;
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
	background-color: white;
`;
const SLink = styled(Link)`
	color: black;
	text-decoration: none;
`;

class SearchResults extends Component {
    componentDidMount() {
        const { category, search } = QueryString.parse(this.props.location.search);

        if (category && search)
            this.props.searchRequest(category, search);
    }

    renderSearchResults = () => {
        if (this.props.error)
            return <ListItem>An error occured, try again later</ListItem>;
        if (this.props.pending)
            return <ListItem>Loading ...</ListItem>;
        if (this.props.results?.length === 0)
            return <ListItem>{`No results for "${this.props.string}" in ${this.props.category}`}</ListItem>;
        if (this.props.results !== null)
            return this.props.results.map(result => {
                const { category, id } = result;
                const name = result.name || result.title;

                return (
                    <SLink to={`/details/${category}/${id}`} key={name}>
                        <ListItem>{name}</ListItem>
                    </SLink>
                );
            });
    }

    render() {
        return (
            <main>
                <List>
                    {this.renderSearchResults()}
                </List>
            </main>
        );
    }
}

const mapStateToProps = state => ({
    ...state.search,
});
const mapDispatchToProps = dispatch => ({
    searchRequest: searchRequestFunc(dispatch),
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchResults));