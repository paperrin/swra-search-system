import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SSelect = styled.select`

`;
const SOption = styled.option`
`;

class Select extends Component {
	renderOptions = () => {
		const options = this.props.options || [];
		return options.map(
			({ value, label }) => (
				<SOption key={value} value={value}>{label}</SOption>
			)
		);
	}

	componentDidMount() {
		if (typeof this.props.onChange === 'function')
			this.props.onChange(this.props.options[0].value);
	}

	onChange = (e) => {
		if (typeof this.props.onChange === 'function')
			this.props.onChange(e.target.value);
	}

	render() {
		return (
			<SSelect className={this.props.className} onChange={this.onChange}>
			{ this.renderOptions() }
			</SSelect>
		);
	}
}

Select.propTypes = {
	onChange: PropTypes.func.isRequired,
	options: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.string.isRequired,
			label: PropTypes.string.isRequired,
		}).isRequired
	).isRequired,
	className: PropTypes.string,
};

export default Select;
