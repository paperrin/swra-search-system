import {
    DETAILS_REQUEST,
    DETAILS_FAILURE,
    DETAILS_SUCCESS,
} from '../constants/details';

import store from 'redux/store';

const detailsRequest = (dispatch, category, id) => {
    const { category: prevCategory, id: prevId } = store.getState().details;
    if (prevCategory === category && prevId === id)
        return {};

    fetch(`/api/category/${category}/${id}`)
        .then(res => res.json())
        .then(data => dispatch(detailsSuccess(data)))
        .catch(error => dispatch(detailsFailure(error)));

    const action = {
        type: DETAILS_REQUEST,
        category,
        id,
    };
    dispatch(action);
    return action;
};

export const detailsFailure = error => ({
    type: DETAILS_FAILURE,
    error,
});

export const detailsSuccess = data => ({
    type: DETAILS_SUCCESS,
    data,
});

export const detailsRequestFunc = dispatch => (category, id) => detailsRequest(dispatch, category, id);
