import {
	SEARCH_REQUEST,
	SEARCH_FAILURE,
	SEARCH_SUCCESS,
} from '../constants/search';

import store from 'redux/store';

const searchRequest = (dispatch, category, string) => {
	const { category: prevCategory, string: prevString } = store.getState().search;
	if (prevCategory === category && prevString === string)
		return {};
	fetch(`/api/category/${category}?search=${string}`)
		.then(res => res.json())
		.then(res => dispatch(searchSuccess(Object.values(res.results))))
		.catch(error => dispatch(searchFailure(error)));

	const action = {
		type: SEARCH_REQUEST,
		category,
		string,
	};
	dispatch(action);
	return action;
};

export const searchFailure = error => ({
	type: SEARCH_FAILURE,
	error,
});

export const searchSuccess = results => ({
	type: SEARCH_SUCCESS,
	results,
});

export const searchRequestFunc = dispatch => (category, string) => searchRequest(dispatch, category, string);
