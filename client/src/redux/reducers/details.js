import {
    DETAILS_REQUEST,
    DETAILS_SUCCESS,
    DETAILS_FAILURE,
} from '../constants/details';

const initialState = {
    data: {},
    pending: false,
    error: null,
};

export default (prevState = initialState, action) => {
    switch (action.type) {
        case DETAILS_REQUEST:
            const { category, id } = action;
            return { ...prevState,
                data: {},
                pending: true,
                error: null,
                category,
                id,
            };
        case DETAILS_FAILURE:
            const { error } = action;
            return { ...prevState,
                data: {},
                pending: false,
                error,
            };
        case DETAILS_SUCCESS:
            const { data } = action;
            return { ...prevState,
                data,
                pending: false,
            };
        default:
            return prevState;
    }
};
