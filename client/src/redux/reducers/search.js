import {
	SEARCH_REQUEST,
 	SEARCH_SUCCESS,
	SEARCH_FAILURE,
} from '../constants/search';

const initialState = {
	results: null,
	pending: false,
	error: null,
};

export default (prevState = initialState, action) => {
	switch (action.type) {
		case SEARCH_REQUEST:
			const { category, string } = action;
			return { ...prevState,
				results: null,
				pending: true,
				error: null,
				category,
				string,
			};
		case SEARCH_FAILURE:
			const { error } = action;
			return { ...prevState,
				results: null,
				pending: false,
				error,
			};
		case SEARCH_SUCCESS:
			const { results } = action;
			return { ...prevState,
				results,
				pending: false,
			};
		default:
			return prevState;
	}
};
