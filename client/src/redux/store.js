import { createStore, combineReducers, compose } from 'redux';
import searchReducer from './reducers/search';
import detailsReducer from './reducers/details';

const reducer = combineReducers({
	search: searchReducer,
	details: detailsReducer,
});

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export default createStore(
	reducer,
	composeEnhancers()
);
