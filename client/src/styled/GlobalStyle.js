import { createGlobalStyle } from 'styled-components';
import colors from './colors.js';

const GlobalStyle = createGlobalStyle`
    select,
    input
    {
        background-color: ${colors.bgColoredOverlay};
        color: ${colors.fontOverlay};
        height: 2em;
        border-radius: 0.2em;
        padding-left: 0.5em;
        padding-right: 0.5em;
        min-width: 5em;
    }
    
    select,
    input,
    button {
        :focus {
            background-color: ${colors.bgColoredOverlayLighter};
        }
    }
    
    button {
        color: ${colors.fontOverlay};
        min-width: 2em;
        min-height: 2em;
        border-radius: 1em 1em 1em 1em;
    }
    
    option {
        color: black;
    }
    
    input::placeholder {
        color: ${colors.fontOverlay};
    }
    
    body {
        background-color: ${colors.bg};
    }
`;

export default GlobalStyle;