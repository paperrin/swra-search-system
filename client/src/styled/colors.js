export default {
    bg: '#eee',
    bgContent: '#fff',
    bgColored: '#b00',
    bgColoredOverlay: 'rgba(255, 255, 255, 0.3)',
    bgColoredOverlayLighter: 'rgba(255, 255, 255, 0.5)',
    font: '#000',
    fontOverlay: '#fff',
};