import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';

import colors from 'styled/colors';
import {detailsRequestFunc} from "../redux/actions/details";
import {connect} from "react-redux";

const Table = styled.table`
	width: 100%;
	background-color: ${colors.bgContent};
`;

const Tr = styled.tr`
	display: flex;
	flex-direction: column;
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
	
	@media screen and (min-width: 30em) {
		flex-direction: row;
		text-align: right;
	}
`;
const Th = styled.th`
	text-align: inherit;
	padding: 10px;
	font-weight: bold;
	text-transform: capitalize;
	min-width: 10em;
`;
const Td = styled.td`
	text-align: left;
	padding: 10px;
	flex: 1;
`;

class Details extends Component {
	componentDidMount() {
		const { category, id } = this.props.match.params;

		this.props.detailsRequest(category, id);
	}

	renderData = () => {
		const { pending, data, error } = this.props;

		if (pending)
			return <Tr><Td>Loading...</Td></Tr>;
		if (error)
			return <Tr><Td>{`Error: ${error.message}`}</Td></Tr>;
		return Object.entries(data)
			.filter(([k, v]) => (!Array.isArray(v) && !isNaN(v)) || typeof v === 'string')
			.map(([k, v]) => {
				const title = k.replace('_', ' ');
				const value = v.length ? v : 'n/a';
				return (
					<Tr key={k}>
						<Th>{title}</Th>
						<Td>{value}</Td>
					</Tr>
				);
			});
	}

	render() {
		return (
			<main>
				<Table>
					<tbody>
						{this.renderData()}
					</tbody>
				</Table>
			</main>
		);
	}
}

const mapStateToProps = state => ({
	...state.details,
});
const mapDispatchToProps = dispatch => ({
	detailsRequest: detailsRequestFunc(dispatch),
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Details));