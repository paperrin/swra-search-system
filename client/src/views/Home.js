import React, { Component } from 'react';

import SearchResults from 'components/SearchResults';

export default class Home extends Component {
    render() {
        return (
            <main>
                <SearchResults />
            </main>
        );
    }
}