const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const routes = require('./routes');

const init = async () => {

	const server = Hapi.server({
		port: 5000,
		host: 'localhost'
	});
	await server.register(Inert);

	server.ext('onPreResponse', (request, h) => {
		const { response } = request;
		if (response instanceof Error)
			server.log('error', response);
		return h.continue;
	});

	server.events.on('log', (event, tags) => {
		if (tags.error) {
			console.log(`Server error: ${event.error ? event.error.message : 'unknown'}`);
		}
	})

	server.route(routes);

	await server.start();
	console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
	console.log(err);
	process.exit(1);
});

init();
