const Hoek = require('@hapi/hoek');
const Boom = require('@hapi/boom');

const swapi = require('../swapi');

module.exports = {
	method: 'GET',
	path: '/api/category/{category}/{id?}',
	handler: async req => {
		const category = Hoek.escapeHtml(req.params.category);
		const id = Hoek.escapeHtml(req.params.id);
		const search = Hoek.escapeHtml(req.query.search);
		let swapiData;

		try {
			if (id)
				swapiData = await swapi.getByCategoryAndId(category, id);
			else if (search)
				swapiData = await swapi.categorySearch(category, search);
			else
				swapiData = await swapi.getCategory(category);
		} catch(e) {
			return Boom.boomify(e, { statusCode: 404 });
		}

		return swapiData;
	},
};