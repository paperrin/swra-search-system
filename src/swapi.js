const axios = require('axios');

const SWAPI_API_URL = 'http://swapi.dev/api';
const SYM_CACHE = Symbol("CACHE");

async function getAll(url) {
 	let response = await axios.get(url)
		.then(res => res.data);
	if (response.next) {
		let nextResponse = await getAll(response.next);
		response.results.push(...nextResponse.results);
	}
	return response;
}

async function getCategories() {
	if (!getCategories[SYM_CACHE]) {
		await axios.get(SWAPI_API_URL)
			.then(res => {
				getCategories[SYM_CACHE] = res.data;
				return res.data;
			})
			.catch(console.error);
		if (!getCategories[SYM_CACHE])
			throw Error('Unable to access swapi');
	}

	return getCategories[SYM_CACHE];
}

async function isValidCategoryName(name) {
	return await getCategories()
		.then(categories => Object.keys(categories).includes(name));
}

async function categorySearch(category, search) {
	if (!await isValidCategoryName(category))
		throw Error('Invalid category');
	if (typeof search !== 'string' || !search.length)
		throw Error('Invalid search string');
	const reqUrl = `${SWAPI_API_URL}/${category}?search=${search}`;

	return convertSwapiData(await getAll(reqUrl));
}

async function getByCategoryAndId(category, id) {
	if (!await isValidCategoryName(category))
		throw Error('Invalid category');
	if (!Number.isInteger(+id) || +id <= 0)
		throw Error('Invalid id');
	const reqUrl = `${SWAPI_API_URL}/${category}/${id}`;

	const data = await axios.get(reqUrl)
		.then(res => res.data);
	return convertSwapiData(data);
}

async function getCategory(category) {
	if (!await isValidCategoryName(category))
		throw Error('Invalid category');
	const reqUrl = `${SWAPI_API_URL}/${category}`;

	return convertSwapiData(await getAll(reqUrl));
}

function convertSwapiData(data, dataKey) {
	if (Array.isArray(data))
		return data.map(e => convertSwapiData(e));
	if (typeof data === 'object') {
		let newObj = mapObject(data, e => convertSwapiData(e));
		if (newObj.url) {
			newObj = {
				...newObj,
				...newObj.url
			};
			delete newObj.url;
		}
		return newObj;
	}
	if (typeof data === 'string' && data.startsWith('http://swapi.dev/api/')) {
		const [ category, id ] = data.split('?')[0].split('/').slice(-3);
		return {
			category,
			id,
		};
	}
	return data;
}

function mapObject(obj, func) {
	const newObj = {};

	for (const key in obj) {
		if (obj.hasOwnProperty(key))
			newObj[key] = func(obj[key], key);
	}

	return newObj;
}

module.exports = {
	getCategories,
	isValidCategoryName,
	categorySearch,
	getByCategoryAndId,
	getCategory,
};
